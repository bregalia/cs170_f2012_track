#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <fcntl.h>

#define SHMSZ     27
void exit();
 
char c;
int shmid;
key_t key=123;
char *shm, *s;

int main(){
  int res=0;
  int i;
  int status=0;
  sem_t *sp;
 
  
  res=fork();
  if (res==0){
    
    /* Locate and attach the segment.  */
    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
        printf("Child: shmget error\n"); exit(1);
    }
    if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) {
        printf("Child: shmat error\n"); exit(1);
    }

    if ((sp = sem_open("mysemaphore", O_CREAT, 0644, 1)) == SEM_FAILED){
        printf("Child: Semphore initialization error\n"); exit(1);
    }
    
    /* Now write and display them*/
    for (i = 0;  i< 10; i++) {
	sem_wait(sp);
      	*shm =     *shm  + 1;
        printf("Child:  shared variable %d \n", (int) *shm); 
	sem_post(sp);
    }

    shmdt(shm);
    sem_close(sp);
    exit(0);
  }
    
  if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
        printf("Parent: shmget error\n"); exit(1);
  }
  if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) {
        printf("Parent: shmat error\n"); exit(1);
  }
  if ((sp = sem_open("mysemaphore", O_CREAT, 0644, 1)) == SEM_FAILED) {
        printf("Parent: Semphore initialization error\n"); exit(1);
  } 
    /* Now write and display them*/
    for (i = 0;  i< 5; i++) {
	sem_wait(sp);
      	*shm =     *shm  + 1;
        printf("Parent:  shared variable %d \n", (int) *shm); 
	sem_post(sp);
    }
 
   wait(&status);
   shmdt(shm);
   sem_close(sp);
   exit(0);
}
