	.file	1 "shell.c"
gcc2_compiled.:
__gnu_compiled_c:
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$fp,128,$31		# vars= 104, regs= 2/0, args= 16, extra= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,128
	sw	$31,124($sp)
	sw	$fp,120($sp)
	move	$fp,$sp
	jal	__main
	sw	$0,20($fp)
	li	$2,1			# 0x1
	sw	$2,24($fp)
	li	$2,45			# 0x2d
	sb	$2,32($fp)
	li	$2,45			# 0x2d
	sb	$2,33($fp)
$L3:
	j	$L5
	j	$L4
$L5:
	addu	$2,$fp,32
	move	$4,$2
	li	$5,2			# 0x2
	lw	$6,24($fp)
	jal	Write
	sw	$0,112($fp)
$L6:
	addu	$2,$fp,48
	lw	$3,112($fp)
	addu	$2,$2,$3
	move	$4,$2
	li	$5,1			# 0x1
	lw	$6,20($fp)
	jal	Read
$L8:
	addu	$2,$fp,112
	lw	$3,0($2)
	addu	$4,$fp,48
	move	$5,$3
	addu	$6,$4,$5
	lb	$4,0($6)
	addu	$3,$3,1
	sw	$3,0($2)
	li	$2,10			# 0xa
	bne	$4,$2,$L9
	j	$L7
$L9:
	j	$L6
$L7:
	lw	$2,112($fp)
	addu	$3,$2,-1
	move	$2,$3
	sw	$2,112($fp)
	addu	$3,$fp,48
	addu	$2,$3,$2
	sb	$0,0($2)
	lw	$2,112($fp)
	blez	$2,$L10
	addu	$2,$fp,48
	move	$4,$2
	jal	Exec
	sw	$2,16($fp)
	lw	$4,16($fp)
	jal	Join
$L10:
	j	$L3
$L4:
$L2:
	move	$sp,$fp
	lw	$31,124($sp)
	lw	$fp,120($sp)
	addu	$sp,$sp,128
	j	$31
	.end	main
