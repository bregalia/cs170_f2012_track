#!/bin/sh

# rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~nelsonchen/public_html/ test/


# Sync Public HTML
rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~cs170/public_html/ cs170 --exclude-from=exclude.lst --delete-excluded
rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~cs170/sample/ sample --exclude-from=exclude.lst --delete-excluded

git add -A
git commit --all -m "Auto Commit from rsync.sh via `hostname` by `whoami`"
git pull
git push 
